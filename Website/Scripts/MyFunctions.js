﻿$(document).ready(function () {

    var number, price, power, search;

    $('#button-add').click(function () {

        number = $("#input-number").val();
        price = $("#input-price").val();
        power = $("#input-power").val();

        $.ajax({
            url: '/Api/Cars/Post',
            type: "POST",
            data: { carNumber: number, price: price, power: power },
            success: function () {
                alert('ok');
            }
        });

    });

    $('#button-search').click(function () {

        search = $("#input-number-search").val();

        $.ajax({
            url: '/Api/Cars/Get',
            type: "GET",
            data: { id: search },
            success: function (data) {
                $("#output-number").val(data.CarNumber);
                $("#output-price").val(data.Price);
                $("#output-power").val(data.Power);
            }
        });

    });
    

});