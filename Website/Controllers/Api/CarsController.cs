﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Website.BusinessLogic.Interfaces;
using Website.DataTransferObjects.Models;

namespace Website.Controllers.Api
{
    public class CarsController : ApiController
    {
        private readonly ICarservice Carservice;

        public CarsController(ICarservice Carservice)
        {
            this.Carservice = Carservice;
        }

        public IHttpActionResult Get(int id)
        {
            return Json(Carservice.GetCarByNumber(id));
        }

        public IHttpActionResult Post(CarDto Car)
        {
            Carservice.AddCar(Car);
            return Ok();
        }
    }
}
