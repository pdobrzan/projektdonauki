﻿using System.Linq;
using Website.BusinessLogic.Interfaces;
using Website.DataAccess.Interfaces;
using Website.DataAccess.Models;
using Website.DataTransferObjects.Models;
using Website.Mapper.Interfaces;

namespace Website.BusinessLogic.Services
{
    public class CarService : ICarservice
    {
        private IEntitiesFactory entitiesFactory;
        private IMapperService mapperService;

        public CarService(IEntitiesFactory entitiesFactory, IMapperService mapperService)
        {
            this.entitiesFactory = entitiesFactory;
            this.mapperService = mapperService;
        }

        public void AddCar(CarDto item)
        {
            using (var context = entitiesFactory.getContext())
            {
                var car = context.Cars.Add(mapperService.Map<Car>(item));
                context.SaveChanges();
            }
        }

        public CarDto GetCarByNumber(int carNumber)
        {
            using (var context = entitiesFactory.getContext())
            {
                var car = context.Cars.FirstOrDefault(a => a.CarNumber == carNumber);
                return mapperService.Map<CarDto>(car);
            }
        }
    }
}
