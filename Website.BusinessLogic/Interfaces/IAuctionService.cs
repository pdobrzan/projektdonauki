﻿using Website.DataTransferObjects.Models;

namespace Website.BusinessLogic.Interfaces
{
    public interface ICarservice
    {
        CarDto GetCarByNumber(int CarNumber);
        void AddCar(CarDto item);
    }
}
