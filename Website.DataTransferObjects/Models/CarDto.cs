﻿namespace Website.DataTransferObjects.Models
{
    public class CarDto
    {
        public int CarNumber { get; set; }
        public int Price { get; set; }
        public int Power { get; set; }
    }
}