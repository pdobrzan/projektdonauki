﻿namespace Website.DataAccess.Models
{
    public class Car
    {
        public int ID { get; set; }
        public int CarNumber { get; set; }
        public int Price { get; set; }
        public int Power { get; set; }
    }
}