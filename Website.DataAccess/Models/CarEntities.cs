﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Website.DataAccess.Models
{
    public class CarEntities : DbContext
    {
        public CarEntities() : base("CarDatabaseContext")
        {
        }

        public virtual DbSet<Car> Cars { get; set; }
    }
}
