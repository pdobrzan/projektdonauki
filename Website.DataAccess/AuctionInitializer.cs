﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Website.DataAccess.Models;

namespace Website.DataAccess
{
    public class CarInitializaer : System.Data.Entity.DropCreateDatabaseIfModelChanges<CarEntities>
    {
        protected override void Seed(CarEntities context)
        {
            var Cars = new List<Car>
            {
            new Car{CarNumber=1, Power=101, Price=11000 },
            new Car{CarNumber=2, Power=200, Price=12000 },
            new Car{CarNumber=3, Power=300, Price=13000 },
            new Car{CarNumber=4, Power=400, Price=14000 },
            new Car{CarNumber=5, Power=500, Price=15000 }
            };

            Cars.ForEach(s => context.Cars.Add(s));
            context.SaveChanges();
        }
    }
}
